import React, { Component } from 'react';
import { connect } from 'react-redux';

import { storage } from '../../../firebase/index';
import Button from '../../../Components/UI/Button/Button';
import Input from '../../../Components/UI/Input/Input';
import Modal from '../../../Components/UI/Modal/Modal';
import * as actions from '../../../store/actions/index';
import classes from './Courses.module.css';

class Courses extends Component {

    state = { 
        showModal: false,
        coursesToDisplay: [],
        controls: {
            courseName: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Course title'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'title'
            },
            courseDescription: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Course description'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'description'
            }
        },
        formIsValid: false,
        selectedFile: null
    }

    componentWillMount() {
        this.props.onCoursefetch(this.props.professorId);
        let courses = [];
        for(let course of this.props.courses) {
            if(course.professorId === this.props.professorId) {
                courses.push(course);
            }
        }
        this.setState({
            coursesToDisplay: courses
        })
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }
        
        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ controls: updatedControls, formIsValid: formIsValid })
    }

    fileSelectHandler = (event) => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    onShowModalHandler = () => {
        let doesModalShow = this.state.showModal;
        this.setState({
            showModal: !doesModalShow
        })
    }

    onCourseAdded = () => {
        const uploadTask = storage.ref(`courses/${this.state.controls.courseName.value + " - " + this.props.professorId}`).put(this.state.selectedFile)
        uploadTask.on('state_changed', 
        (snapshot) => {}, (error) => {}, 
        () => {
            storage.ref('courses').child(this.state.controls.courseName.value + " - " + this.props.professorId).getDownloadURL().then(url => {
                let dataToEntered = {
                    courseName: this.state.controls.courseName.value,
                    courseDescription: this.state.controls.courseDescription.value,
                    professorId: this.props.professorId,
                    imageURL: url
                };
                this.props.onCourseAdded(dataToEntered);
                this.onShowModalHandler();
            })
        })
    }

    render() {

        let courses = this.state.coursesToDisplay.map(course => {
            return (
                <div className = {classes.Courses} key = {course.id}>
                    <img src = {course.imageURL} alt = "course" width ="500px" height = "300px"/>
                    <p>Course Title: {course.courseName}</p>
                    <p>Course Description: {course.courseDescription}</p>
                    <p>Course Content: Contains 11 modules</p>
                </div>
            )
        })

        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        } 

        let form = formElementArray.map( formElement => (
                        <Input 
                            style = {{
                                background: "transparent",
                                border: "none",
                                borderBottom: "1px solid rgba(75, 73, 73, 0.5)",
                                color: "black"
                            }}
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    )
        );

        return (
            <div className = {classes.CourseBody}>
                <Modal show = {this.state.showModal} clicked = {this.onShowModalHandler}>
                    {form}
                    <input type="file" onChange = {(event) => this.fileSelectHandler(event)}/>
                    <Button
                        style = {{
                            display: 'flex',
                            width: '100%',
                            justifyContent: 'center'
                        }}
                        clicked = {() => this.onCourseAdded()}>Add course</Button>
                </Modal>
                <Button 
                    style = {{
                        display: 'flex',
                        width: '95%',
                        justifyContent: 'flex-end'
                    }}
                    clicked = {this.onShowModalHandler}>ADD NEW COURSE</Button>
                <div>
                    {courses}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.professor.courses,
        professorId: state.home.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onCoursefetch: (professorId) => dispatch(actions.onCoursesFetch(professorId)),
        onCourseAdded: (data) => dispatch(actions.onCourseAdd(data)) 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Courses);