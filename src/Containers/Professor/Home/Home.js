import React, { Component } from 'react';

import classes from './Home.module.css';

class Home extends Component {
    state = {  }
    render() {
        return (
            <div>
                <div className = {classes.Home}>
                    <div className = {classes.HomeContent}>
                        <h3>Welcome back to Study Mania</h3>
                        <p>Part time online Teaching</p>
                    </div>
                </div>
                <div className = {classes.Notifications}>
                    <p>Notifications</p>
                </div>
            </div>
        );
    }
}

export default Home;