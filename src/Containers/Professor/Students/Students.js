import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../../store/actions/index';
import Button from '../../../Components/UI/Button/Button';
import classes from './Students.module.css';

class Students extends Component {
    state = { 
        studentsToBeViewed: []
    }

    componentWillMount() {
        this.props.onStudentAddedCourseFetch();
        let courses = [];
        for(let course of this.props.coursesAddedByStudents) {
            if(course.professorId === this.props.professorId) {
                courses.push(course);
            }
        }
        this.setState({
            studentsToBeViewed: courses
        });
    }

    render() {
        let students = this.state.studentsToBeViewed.map(student => {
            return (
                <div className = {classes.Students}>
                    <div>
                        <img src = "https://cdn4.iconfinder.com/data/icons/eldorado-user/40/user-512.png" alt = "student profile"  width = "150px" height = "150px"/>
                    </div>
                    <div className = {classes.StudentsMain}>
                        <p>Students Details : {student.studentId}</p>
                        <p>Student name: Jhon</p>
                        <Button 
                            style ={{
                                display: 'flex',
                                justifyContent: 'flex-end',
                                width: '100%'
                            }}>DELETE</Button>
                        </div>
                </div>
            )
        })
        return (
            <div>
                {students}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.professor.courses,
        coursesAddedByStudents: state.professor.studentAddedCourses,
        professorId: state.home.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onStudentAddedCourseFetch: () => dispatch(actions.onCoursesStudentsAdded())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Students);