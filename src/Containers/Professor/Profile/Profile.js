import React, { Component } from 'react';

import Button from '../../../Components/UI/Button/Button';
import classes from './Profile.module.css';

class Profile extends Component {
    render() {
        return (
            <div>
               <div className = {classes.ProfileBackground}>
                    <div className = {classes.ProfileMain}>
                        <img src = "https://cdn4.iconfinder.com/data/icons/eldorado-user/40/user-512.png" alt = "profile"  width = "150px" height = "150px"/>
                        <Button>Upload picture</Button>
                    </div>
                    <div className = {classes.ProfileMainContent}>
                        <h2>Prashanth Kumar Thallada</h2>
                        <p>FreeLancer at Study Mania</p>
                        <p><strong>Location</strong> Maryville, MO</p>
                        <p><strong>Bio</strong> developed many websites using MERN stack</p>
                    </div>
               </div>
               <div className = {classes.ProfileBackground2}>
                   <p className = {classes.About}>About</p>
                   <div className = {classes.ProfileContent}>
                       <label>Email</label>
                       <p>prashanththallada2696@gmail.com</p>
                   </div>
                   <div className = {classes.ProfileContent}>
                       <label>Business Phone</label>
                       <p>(660) 528-0453</p>
                   </div>
               </div>
            </div>
        );
    }
}

export default Profile;