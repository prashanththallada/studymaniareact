import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './Home.module.css';
import * as actions from '../../../store/actions/index';

class Home extends Component {
    state = { 
        coursesToBeViewed: []
    }

    componentWillMount() {
        this.props.onCourseForStudentFetch(this.props.studentId);
        let courses = [];
        for(let course of this.props.courses) {
            if(course.payment) {
                courses.push(course);
            }
        }
        this.setState({
            coursesToBeViewed: courses
        });
    }

    render() {
        let courses = this.state.coursesToBeViewed.map(course => {
            return (
                <div className = {classes.Course} key = {course.id}>
                    <img src = {course.imageURL} alt = "course" width = "150px" height = "100px"/>
                    <p>Course title: {course.courseTitle}</p>
                    <p>Professor details: {course.professorId}</p>
                    <p>Course Description: {course.courseDescription}</p>
                </div>
            )
        })

        let counter = 0;

        for(let data of courses) {
            if(data === null) {
                counter++;
            } else {
                break;
            }
        }

        if(counter === courses.length) {
            courses = (
                <div style = {{
                    display: 'flex',
                    justifyContent: 'center',
                    width: '100%'
                }}>
                    <p>You havent added any course yet, start adding them in avaialble courses.</p>
                </div>
            )
        }

        return (
            <div>
                <div className = {classes.Home}>
                    <div className = {classes.HomeContent}>
                        <h3>Welcome back to Study Mania</h3>
                        <p>An investment in knowledge pays the best interest</p>
                    </div>
                </div>
                <div className = {classes.Notifications}>
                    {courses}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.student.coursesOnCart,
        studentId: state.home.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onCourseForStudentFetch: (studentId) => dispatch(actions.onStudentCoursesCartFetched(studentId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);