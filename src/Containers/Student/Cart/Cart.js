import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../../store/actions/index'; 
import Button from '../../../Components/UI/Button/Button';
import classes from './Cart.module.css';

class Cart extends Component {
    state = { 
        cartToBeViewed: []
    }

    componentWillMount() {
        this.props.onCoursesinCartFetched(this.props.studentId);
        this.props.onCourseForStudentFetch(this.props.studentId);
        let courseToBeViewed = [];
        for(let course of this.props.courses) {
            if(course.addedToCart) {
                courseToBeViewed.push(course);
            }
        }
        this.setState({
            cartToBeViewed: courseToBeViewed
        });
    }

    onPayment = (data) => {
        let dataToBeUpdated = {
            ...data,
            payment: true
        }
        let index = this.state.cartToBeViewed.findIndex(course => course.id === data.id);
        let updatedCourses = [
            ...this.state.cartToBeViewed
        ];
        updatedCourses[index] = dataToBeUpdated;
        this.setState({
            cartToBeViewed: updatedCourses
        });
        this.props.onPaymentDone(data.id, dataToBeUpdated);
    }

    render() {

        let courses = this.state.cartToBeViewed.map(course => {
            return (
                <div key = {course.id} className = {classes.Cart}>
                    <p>Course title: {course.couseTitle}</p>
                    <p>Professor Details: {course.professorId}</p>
                    <p>Course Details: {course.courseId}</p>
                    {course.payment ? <p>PAYMENT ACCEPTED</p> : <Button clicked = {() => this.onPayment(course)}>PAY</Button>}
                </div>
            )
        })

        return (
            <div>
                {courses}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.student.coursesOnCart,
        studentId: state.home.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onCoursesinCartFetched: (studentId) => dispatch(actions.onStudentCoursesFetch(studentId)),
        onCourseForStudentFetch: (studentId) => dispatch(actions.onStudentCoursesCartFetched(studentId)),
        onPaymentDone: (cartId, data) => dispatch(actions.onStudentCoursesCartPayed(cartId, data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);