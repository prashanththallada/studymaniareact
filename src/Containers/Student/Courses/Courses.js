import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './Courses.module.css';
import Button from '../../../Components/UI/Button/Button';
import * as actions from '../../../store/actions/index';

class Courses extends Component {
    state = {  
        coursesToDisplay: []
    }

    componentWillMount() {
        this.props.onCoursefetch();
        this.setState({
            coursesToDisplay: this.props.courses
        })
    }

    onAddToCart = (data) => {
        let dataToBeAdded = {
            courseTitle: data.courseName,
            courseId: data.id,
            imageURL: data.imageURL,
            courseDescription: data.courseDescription,
            professorId: data.professorId,
            studentId: this.props.studentId,
            addedToCart: true
        };
        let course = this.state.coursesToDisplay.findIndex(course => course.id === data.id);
        let updatedCourse = {
            ...this.state.coursesToDisplay[course],
            addedToCart: true
        };
        let courses = [
            ...this.state.coursesToDisplay
        ]
        courses[course] = updatedCourse;
        this.setState({
            coursesToDisplay: courses
        })
        this.props.onStudentsAddingCourses(dataToBeAdded);
    }

    render() {
        let courses = this.state.coursesToDisplay.map(course => {
            return (
                <div className = {classes.Courses} key = {course.id}>
                    <img src = {course.imageURL} alt = "course" width ="500px" height = "300px"/>
                    <p>Course Title: {course.courseName}</p>
                    <p>Course Description: {course.courseDescription}</p>
                    <p>Course Content: Contains 11 modules</p>
                    {course.addedToCart ? <p>ADDED TO CART</p> : <Button clicked = {() => this.onAddToCart(course)}>ADD TO CART</Button>}
                </div>
            )
        })
        
        return (
            <div>
                {courses}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.professor.courses,
        studentId: state.home.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onCoursefetch: () => dispatch(actions.onStudentCoursesFetch()),
        onStudentsAddingCourses: (data) => dispatch(actions.onStudentCoursesAdded(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Courses);