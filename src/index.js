import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';


import App from './App';
import './index.css';
import * as serviceWorker from './serviceWorker';
import HomeReducer from './store/reducer/homeReducer';
import ProfessorReducer from './store/reducer/professorReducer';
import StudentReducer from './store/reducer/studentreducer';
import thunk from 'redux-thunk';

const rootreducer = combineReducers({
    home: HomeReducer,
    professor: ProfessorReducer,
    student: StudentReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootreducer, composeEnhancers(
    applyMiddleware(thunk)
));

ReactDOM.render(<Provider store = {store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
