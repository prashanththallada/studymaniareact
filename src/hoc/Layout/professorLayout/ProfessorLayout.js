import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import classes from './ProfessorLayout.module.css';
import ProfessorNavigations from '../../../Components/Navigations/Professors/professorNavigation';
import ProfessorHome from '../../../Containers/Professor/Home/Home';
import ProfessorProfile from '../../../Containers/Professor/Profile/Profile';
import ProfessorCourses from '../../../Containers/Professor/Courses/Courses';
import ProfessorStudentsReview from '../../../Containers/Professor/Students/Students';

class ProfessorLayout extends Component {
    render() {
        return (
            <div>
                <div className = {classes.ProfessorLayoutNavigations}>
                    <ProfessorNavigations />
                </div>
                <div className = {classes.ProfessorLayoutContent}>
                    <Switch>
                        <Route path = "/professor/courses" component = {ProfessorCourses}/>
                        <Route path = "/professor/manage" component = {ProfessorStudentsReview}/>
                        <Route path = "/professor/profile" component = {ProfessorProfile}/>
                        <Route path = "/professor" component = {ProfessorHome}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default ProfessorLayout;