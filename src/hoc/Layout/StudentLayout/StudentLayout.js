import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import StudentNavigations from '../../../Components/Navigations/Students/studentNavigation';
import StudentHome from '../../../Containers/Student/Home/Home';
import StudentProfile from '../../../Containers/Student/Profile/Profile';
import StudentCourses from '../../../Containers/Student/Courses/Courses';
import StudentCart from '../../../Containers/Student/Cart/Cart';
import classes from './StudentLayout.module.css';

class StudentLayout extends Component {
    render() {
        return (
            <div>
                <div className = {classes.StudentLayoutNavigations}>
                    <StudentNavigations />
                </div>
                <div className = {classes.StudentLayoutContent}>
                    <Switch>
                        <Route path = "/student/courses" component = {StudentCourses}/>
                        <Route path = "/student/cart" component = {StudentCart}/>
                        <Route path = "/student/profile" component = {StudentProfile}/>
                        <Route path = "/student" component = {StudentHome}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default StudentLayout;