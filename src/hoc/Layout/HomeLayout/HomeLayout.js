import React, { Component } from 'react';
import { connect } from 'react-redux';

import HomeNavigations from '../../../Components/Navigations/Home/homeNavigations';
import classes from './HomeLayout.module.css';
import Input from '../../../Components/UI/Input/Input';
import Button from '../../../Components/UI/Button/Button';
import * as actions from '../../../store/actions/index';
import Aux from '../../Auxiliary/Auxiliary';

class HomeLayout extends Component {

    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                valueType: 'eMail'
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                valueType: 'password'
            }
        },
        formIsValid: false
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }
        
        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ controls: updatedControls, formIsValid: formIsValid })
    }

    onLoggedIn = (event, type, mode) => {
        event.preventDefault();
        let typeOfLogin = true;
        if(mode) {
            if(type === "professor") {
                this.props.onLoggedIn(this.state.controls.email.value, this.state.controls.password.value, !mode, typeOfLogin);
                // this.props.history.push("/professor");
            } else {
                typeOfLogin = false;
                this.props.onLoggedIn(this.state.controls.email.value, this.state.controls.password.value, !mode, typeOfLogin);
                this.props.history.push("/student");
            }
        } else {
            this.props.onLoggedIn(this.state.controls.email.value, this.state.controls.password.value, true, false);
            this.props.onLoginModeChanged(true);
        }
    }




    render() {

        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        } 

        let loginType = null;
        if(this.props.loginType === "professor") {
            loginType = <p>Professor LOGIN</p>
        } else {
            loginType = <p>Student LOGIN</p>
        }

        let loginMode = null;
        if(this.props.loginMode) {
            loginMode = (
                <div>
                    <p className = {classes.ForgotPassword} style ={{ fontSize: "0.8rem", margin: "0px" }}>Forgot password?</p>
                    <Button 
                        style = {{
                            width: "100%",
                            margin: "auto",
                            background: "seagreen",
                            color: "ghostwhite",
                            borderRadius: "10px",
                            marginTop: "16px" 
                        }} 
                        disabled = {!this.state.formIsValid}> LOGIN </Button> 
                    <p className = {classes.SignUp} style ={{ fontSize: "0.8rem", margin: "0px" }}>New to Study Mania, <span onClick = {() => this.props.onLoginModeChanged(false)}>SignUp</span></p>
                </div>
            )
        } else {
            loginMode = (
                <div>
                    <Button 
                        style = {{
                            width: "100%",
                            margin: "auto",
                            background: "seagreen",
                            color: "ghostwhite",
                            borderRadius: "10px",
                            marginTop: "16px" 
                        }} 
                        disabled = {!this.state.formIsValid}> SIGNUP </Button> 
                </div>
            )
        }

        let form = ( 
            <form onSubmit = {(event) => this.onLoggedIn(event, this.props.loginType, this.props.loginMode)}>
                {
                    formElementArray.map( formElement => (
                        <Input 
                            style = {{
                                background: "transparent",
                                border: "none",
                                borderBottom: "1px solid rgba(75, 73, 73, 0.5)",
                                color: "black"
                            }}
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))
                }
                {loginMode}
            </form>
        );

        let errorMessage = null;

        if(this.props.error) {
            errorMessage = <p style = {{fontSize: "1rem", fontWeight: "300"}} >{this.props.error.message}</p>
        }

        let homePageDisplay = null;
        if(this.props.homePage === "home") {
            homePageDisplay = (
                <Aux>
                    <div className = {classes.Content}>
                        <h3>Online Learning and Teaching Market</h3>
                        <p>The world’s largest selection of courses </p>
                        <p>Choose from over 100,000 online video courses with new additions published every month</p>
                    </div>
                    <div className = {classes.Login}>
                        {errorMessage}
                        {loginType}
                        {form}
                    </div>
                </Aux>
            )
        } else {
            homePageDisplay = (
                <div className = {classes.Content2}>
                    <h3>Online Learning and Teaching Market</h3>
                    <p>Welcome to Study Mania, your number one source for E-learning. We're dedicated to giving you the very best of our online courses, with a focus on dependability, customer service and uniqueness.</p>
                    <p>Founded in 2019 by team studyMania, It has come a long way from its beginnings in a Maryville, MO. When we first started out, our passion for helping others to grow their knowledge on their desired aspects. We now serve customers all over the US and are thrilled to be a part of the quirky, eco-friendly, fair trade wing of the education industry.</p>
                    <p>We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.</p>
                    <p>Sincerely,</p>
                    <p>Team StudyMania</p>
                </div>
            );
        }

        return (
            <div>
                <HomeNavigations />
                <div className = {classes.Body}>
                    {homePageDisplay}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loginType: state.home.loginType,
        loginMode: state.home.loginMode,
        error: state.home.error,
        homePage: state.home.homePage
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLoginModeChanged: (mode) => dispatch(actions.onLoginModeChanged(mode)),
        onLoggedIn: (email, password, isSignup, isAdmin) => dispatch(actions.auth(email, password, isSignup, isAdmin))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout);