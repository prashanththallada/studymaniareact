import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://online-teaching-react.firebaseio.com/'
});

export default instance;