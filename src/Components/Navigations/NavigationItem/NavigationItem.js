import React from 'react';
import { NavLink } from 'react-router-dom';

import classes from './NavigationItem.module.css'

const NavigationItem = (props) => {
    return (
        <div>
            <NavLink
                to = {props.link}
                exact
                activeClassName = {classes.ProfessorNavigations}>{props.children}</NavLink>
        </div>
    );
}

export default NavigationItem;