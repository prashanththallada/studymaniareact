import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavigationItem from '../NavigationItem/NavigationItem';
import classes from './ProfessorNavigations.module.css';
import * as actions from '../../../store/actions/index';
import Button from '../../UI/Button/Button';

class ProfessorNavigation extends Component {
    render() {
        return (
            <div>
                <nav className = {classes.ProfessorNavigations}>
                    <NavigationItem link = "/professor">HOME</NavigationItem>
                    <NavigationItem link = "/professor/courses">COURSES</NavigationItem>
                    <NavigationItem link = "/professor/manage">MANAGE STUDENTS</NavigationItem>
                    <NavigationItem link = "/professor/profile">PROFILE</NavigationItem>
                    <NavigationItem link = "/"><Button style={{
                        margin: "0px",
                        padding: "0px",
                    }}
                    clicked = {this.props.onAuthLogout}>LOGOUT</Button></NavigationItem>
                </nav>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuthLogout: () => dispatch(actions.authLogout())
    }
}

export default connect(null, mapDispatchToProps)(ProfessorNavigation);