import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavigationItem from '../NavigationItem/NavigationItem';
import classes from './HomeNavigations.module.css';
import logo from '../../../assets/Logo-removebg.png';
import * as actions from '../../../store/actions/index';

class HomeNavigations extends Component {

    state = {
        showLogin: false
    }

    onLoginHandler = () => {
        let doesShowlogin = this.state.showLogin
        this.setState({
            showLogin: !doesShowlogin
        })
    }

    onLoginClickedHandler = (type) => {
        this.props.onLoginTypeChanged(type);
    }

    onHomePageHandler = (type) => {
        this.props.onHomePageChanged(type);
    }

    render() {
        let loginDetails = null;
        if(this.state.showLogin) {
            loginDetails = (
                <div className = {[classes.HomeNavigationContent, classes.Body, classes.Navigations].join(' ')}>
                    <p onClick = {() => this.onLoginClickedHandler("professor")}>PROFESSOR</p>
                    <p onClick = {() => this.onLoginClickedHandler("student")}>STUDENT</p>
                </div>
            )
        }
        return (
            <div className = {classes.HomeComponent}>
                <div className = {classes.Logo}>
                    <img src={logo} alt="logo" width= "120px" height = "120px"/>
                </div>
                <div className = {classes.HomeNavigations}>
                    <div className = {[classes.HomeNavigationContent, classes.Home].join(' ')}>
                        Student Mania
                    </div>
                    <div className = {[classes.HomeNavigationContent, classes.Body].join(' ')}>
                        <p onClick = {() => this.onHomePageHandler("home")}><NavigationItem>HOME</NavigationItem></p>
                        <p 
                            onClick = {(e) => {
                                this.onLoginHandler(e);
                                this.onHomePageHandler("home");
                            }}><NavigationItem>LOGIN</NavigationItem></p>
                        <p onClick = {() => this.onHomePageHandler("about")}><NavigationItem>ABOUT</NavigationItem></p>
                    </div>
                    {loginDetails}
                </div>
                <div className = {classes.Input}>
                    <input type="text" placeholder="Search"/>
                    <i class="fa fa-search" style={{ fontSize: "1.2rem" }}></i>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loginType: state.home.loginType 
    };
};

const mapDispatchtoprops = dispatch => {
    return {
        onLoginTypeChanged: (loginType) => dispatch(actions.onLoginTypeChanged(loginType)),
        onHomePageChanged: (type) => dispatch(actions.onHomePageChanged(type))
    };
};

export default connect(mapStateToProps, mapDispatchtoprops)(HomeNavigations);