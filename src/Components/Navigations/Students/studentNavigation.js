import React, { Component } from 'react';
import { connect } from 'react-redux';
 
import NavigationItem from '../NavigationItem/NavigationItem';
import classes from './studentNavigations.module.css';
import * as actions from '../../../store/actions/index';
import Button from '../../UI/Button/Button';

class StudentNavigation extends Component {
    render() {
        return (
            <div>
                <nav className = {classes.StudentNavigations}>
                    <NavigationItem link = "/student">HOME</NavigationItem>
                    <NavigationItem link = "/student/courses">AVAILABLE COURSES</NavigationItem>
                    <NavigationItem link = "/student/cart">CART</NavigationItem>
                    <NavigationItem link = "/student/profile">PROFILE</NavigationItem>
                    <NavigationItem link = "/"><Button style={{
                        margin: "0px",
                        padding: "0px",
                    }}
                    clicked = {this.props.onAuthLogout}>LOGOUT</Button></NavigationItem>
                </nav>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuthLogout: () => dispatch(actions.authLogout())
    }
}

export default connect(null, mapDispatchToProps)(StudentNavigation);