import React from 'react';
import classes from './Input.module.css';

const input = (props) => {

    let inputtype = null

    let inputClasses = [classes.InputElement];
    if(props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }

    let validationError = null;
    if (props.invalid && props.touched) {
        validationError = <p  style = {{fontSize: "1rem", fontWeight: "300"}} className={classes.ValidationError}>Please enter a valid {props.valueType}!</p>;
    }    

    switch (props.inputtype) {
        case 'input':
            inputtype = <input 
                            className = {inputClasses.join(' ')} 
                            style = {props.style}
                            {...props.elementConfig} 
                            value = {props.value}
                            onChange = {props.changed}/>;
            break;
        case 'select':
            inputtype =  <select 
                            className = {inputClasses.join(' ')}
                            value = {props.value}
                            onChange = {props.changed}> 
                            {props.options.map(option => (
                                <option key = {option.value} value = {option.value}>
                                    {option.displayValue}
                                </option>
                            ))}
                        </select>;
            break;
        default :
            inputtype = <input 
                            className = {inputClasses.join(' ')} 
                            {...props.elementConfig} 
                            style = {props.style}
                            value = {props.value}
                            onChange = {props.changed}/>;
    }

    return (
        <div className = {classes.Input}>
            {inputtype}
            {validationError}
        </div>
    )
};

export default input;