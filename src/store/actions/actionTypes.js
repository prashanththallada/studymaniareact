export const  LOGIN_TYPE_CHANGE = "LOGIN_TYPE_CHANGE";
export const LOGIN_MODE_CHANGED = "LOGIN_MODE_CHANGED";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const HOME_PAGE_CHANGED = "HOME_PAGE_CHANGED";

export const COURSE_ADDED = "COURSE_ADDED";
export const COURSES_FETCHED = "COURSES_FETCHED";
export const COURSE_FETCHED_STUDENTS = "COURSE_FETCHED_STUDENTS";

export const COURSE_ADDED_CART = "COURSE_ADDED_CART";
export const COURSE_CART_FETCHED = "COURSE_CART_FETCHED";
export const COURSE_PAYMENT_DONE = "COURSE_PAYMENT_DONE";