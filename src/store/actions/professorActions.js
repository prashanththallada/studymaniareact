import * as actionTypes from './actionTypes';
import axios from '../../axios-online-teaching';

export const onCourseAdded = (data) => {
    return {
        type: actionTypes.COURSE_ADDED,
        data: data
    }
}

export const onCourseAdd = (data) => {
    return dispatch => {
        axios.post('/professor.json', data)
            .then(response => {
                dispatch(onCourseAdded(data));
            })
    }
}

export const onCoursesFetched = (data) => {
    return {
        type: actionTypes.COURSES_FETCHED,
        data: data
    }
}

export const onCoursesFetch = (professorId) => {
    return dispatch =>  {
        axios.get('/professor.json')
            .then(response => {
                let fetchedCourses = [];
                for(let course in response.data) {
                    if(response.data[course].professorId === professorId) {
                        fetchedCourses.push({
                            ...response.data[course],
                            id: course
                        });
                    }
                }
                dispatch(onCoursesFetched(fetchedCourses));
            })
    }
}

export const onCourseStudentFetch = (data) => {
    return {
        type: actionTypes.COURSE_FETCHED_STUDENTS,
        data: data
    }
}

export const onCoursesStudentsAdded = () => {
    return dispatch => {
        axios.get('/student.json')
            .then(response => {
                let fetchedCourses = [];
                for(let course in response.data) {
                    fetchedCourses.push({
                        ...response.data[course],
                        id: course
                    });
                }
                dispatch(onCourseStudentFetch(fetchedCourses));
            })
    }
}