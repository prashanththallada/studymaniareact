export {
    onLoginTypeChanged,
    onLoginModeChanged,
    auth,
    authCheckState,
    authLogout,
    onHomePageChanged
} from './homeActions';

export {
    onCoursesFetch,
    onCourseAdd,
    onCoursesStudentsAdded
} from './professorActions';

export {
    onStudentCoursesFetch,
    onStudentCoursesAdded,
    onStudentCoursesCartPayed,
    onStudentCoursesCartFetched
} from './studentActions';