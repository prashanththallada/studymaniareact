import * as actionTypes from './actionTypes';
import axios from '../../axios-online-teaching';

export const onCoursesFetched = (data) => {
    return {
        type: actionTypes.COURSES_FETCHED,
        data: data
    }
}

export const onStudentCoursesFetch = () => {
    return dispatch =>  {
        axios.get('/professor.json')
            .then(response => {
                let fetchedCourses = [];
                for(let course in response.data) {
                    fetchedCourses.push({
                        ...response.data[course],
                        id: course
                    });
                }
                dispatch(onCoursesFetched(fetchedCourses));
            })
    }
}

export const onStudentAddingCourse = (data) => {
    return {
        type: actionTypes.COURSE_ADDED_CART,
        data: data
    }
}

export const onStudentCoursesAdded = (data) => {
    return dispatch => {
        axios.post('/student.json', data)
            .then(response => {
                dispatch(onStudentAddingCourse(data));
            })
    }
}

export const onStudentCoursesCartFetch = (data) => {
    return {
        type: actionTypes.COURSE_CART_FETCHED,
        data: data
    }
}

export const onStudentCoursesCartFetched = (studentID) => {
    return dispatch => {
        axios.get('/student.json')
            .then(response => {
                let fetchedCoursesInCart = [];
                for(let course in response.data) {
                    if(response.data[course].studentId === studentID) {
                        fetchedCoursesInCart.push({
                            ...response.data[course],
                            id: course
                        });
                    }
                }
                dispatch(onStudentCoursesCartFetch(fetchedCoursesInCart));
            })
    }
}

export const onStudentCoursePayed = (cartId, data) => {
    return {
        type: actionTypes.COURSE_PAYMENT_DONE,
        payload: {
            id: cartId,
            data: data
        }
    }
}

export const onStudentCoursesCartPayed = (cartId, data) => {
    return dispatch => {
        axios.put('/student/' + cartId + '.json', data)
            .then(response => {
                dispatch(onStudentCoursePayed(cartId, data));
            })
    }
}