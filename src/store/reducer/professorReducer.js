import * as actionTypes from '../actions/actionTypes';

const initialState = {
    courses: [],
    studentAddedCourses: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COURSES_FETCHED: 
            return {
                ...state,
                courses: action.data
            }
        case actionTypes.COURSE_ADDED: 
            return {
                ...state,
                courses: state.courses.concat(action.data)
            }
        case actionTypes.COURSE_FETCHED_STUDENTS:
            return {
                ...state,
                studentAddedCourses: action.data
            }
        default:
            return state
    }
};

export default reducer;