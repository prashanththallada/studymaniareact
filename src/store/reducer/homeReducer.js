import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loginType: "professor",
    homePage: "home",
    loginMode: true,
    userId: null,
    token: null,
    error: null,
    admin: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_TYPE_CHANGE:
            return {
                ...state,
                loginType: action.loginType
            }
        case actionTypes.LOGIN_MODE_CHANGED:
            return {
                ...state,
                loginMode: action.mode
            }
            case actionTypes.AUTH_SUCCESS: 
            return {
                ...state,
                userId: action.userId,
                token: action.tokenId,
                admin: action.admin
            }
        case actionTypes.AUTH_LOGOUT:
            return {
                ...state,
                userId: null,
                token: null,
                admin: false
            }
        case actionTypes.AUTH_FAIL:
            return {
                ...state,
                error: action.error
            }
        case actionTypes.HOME_PAGE_CHANGED:
            return {
                ...state,
                homePage: action.mode
            }
        default: 
            return state;
    }
}

export default reducer;