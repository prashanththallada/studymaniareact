import * as actionTypes from '../actions/actionTypes';

const initialState = {
    courses: [],
    coursesOnCart: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COURSES_FETCHED: 
            return {
                ...state,
                courses: action.data
            }
        case actionTypes.COURSE_ADDED_CART:
            return {
                ...state,
                coursesOnCart: state.coursesOnCart.concat(action.data)
            }
        case actionTypes.COURSE_CART_FETCHED:
            return {
                ...state,
                coursesOnCart: action.data
            }
        case actionTypes.COURSE_PAYMENT_DONE:
            let updatedCourses = [
                ...state.coursesOnCart
            ]
            let courseIndex = state.coursesOnCart.findIndex(course => course.id === action.payload.id);
            updatedCourses[courseIndex] = action.data;
            return {
                ...state,
                coursesOnCart: updatedCourses
            }
        default:
            return state
    }
};

export default reducer;