import firebase from 'firebase/app';
import 'firebase/storage';

var config = {
    apiKey: "AIzaSyCehpH5r_CILEzqnUPtXtdvE0PI9iXwZCU",
    authDomain: "online-teaching-react.firebaseapp.com",
    databaseURL: "https://online-teaching-react.firebaseio.com",
    projectId: "online-teaching-react",
    storageBucket: "online-teaching-react.appspot.com",
    messagingSenderId: "683863606097"
};

firebase.initializeApp(config);

const storage = firebase.storage();
export {
    storage, firebase as default
};