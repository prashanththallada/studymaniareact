import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import ProfessorLayout from './hoc/Layout/professorLayout/ProfessorLayout';
import HomeLayout from './hoc/Layout/HomeLayout/HomeLayout';
import StudentLayout from './hoc/Layout/StudentLayout/StudentLayout';
import * as actions from './store/actions/index';

class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {

    let router = (
      <Switch>
        <Route path="/" component = {HomeLayout} />
        <Redirect to = "/"/>
      </Switch>
    )

    if(this.props.isAuthenticated) {
      if (this.props.admin) {
        router = (
          <Switch>
            <Route path="/professor" component = {ProfessorLayout} />
            <Redirect to = "/professor"/>
          </Switch>
        )
      } else {
        router = (
          <Switch>
            <Route path="/student" component = {StudentLayout} />
            <Redirect to = "/student"/>
          </Switch>
        )
      }
    }

    return (
      <BrowserRouter>
        <div>
          {router}
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.home.token !== null,
    admin: state.home.admin
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
